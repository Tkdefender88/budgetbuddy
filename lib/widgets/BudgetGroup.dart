import 'package:flutter/material.dart';
import 'package:money_matters/data.dart';
import 'package:money_matters/widgets/BudgetItem.dart';

class BudgetGroup extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Container(
      decoration: BoxDecoration(
        boxShadow: customShadow,
        borderRadius: BorderRadius.all(Radius.circular(10)),
        color: Colors.white,
      ),
      margin: EdgeInsets.only(left: 20, right: 20, top: 24),
      padding: EdgeInsets.symmetric(horizontal: 15),
      child: Stack(
        overflow: Overflow.visible,
        children: <Widget>[
          Positioned(
            top: -15,
            child: Container(
              padding: EdgeInsets.all(5.0),
              decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(10),
                color: Colors.deepOrangeAccent,
              ),
              child: Text(
                'Fixed Expenses',
                style: TextStyle(fontSize: 16, fontWeight: FontWeight.bold),
              ),
            ),
          ),
          Container(
            margin: EdgeInsets.only(top: 15),
            child: Column(
              children: [
                BudgetItem(),
                BudgetItem(),
                BudgetItem(),
                IconButton(
                  icon: Icon(Icons.add),
                  color: Colors.deepOrange,
                  onPressed: () {},
                ),
              ],
            ),
          ),
        ],
      ),
    );

    /*Column(
      children: <Widget>[
        Container(
          decoration: BoxDecoration(
            boxShadow: customShadow,
            borderRadius: BorderRadius.all(Radius.circular(10)),
            color: Colors.white70,
          ),
          margin: EdgeInsets.symmetric(horizontal: 20),
          padding: EdgeInsets.symmetric(horizontal: 15),
          child: Column(
            children: <Widget>[
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: <Widget>[
                  Text("Group",
                      style:
                          TextStyle(fontSize: 18, fontWeight: FontWeight.bold)),
                  Text("Remaining", style: TextStyle(fontSize: 18)),
                ],
              ),
              Row(children: [
                BudgetItem(),
              ]),
              Row(mainAxisAlignment: MainAxisAlignment.end, children: [
                IconButton(
                  icon: Icon(Icons.add, color: Colors.deepOrange),
                  onPressed: () {},
                )
              ])
            ],
          ),
        ),
      ],
    );*/
  }
}
