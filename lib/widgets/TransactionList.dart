import 'package:flutter/material.dart';
import 'package:money_matters/widgets/Transaction.dart';

class TxList extends StatefulWidget{
    @override
    _TxListState createState() => _TxListState();
}

class _TxListState extends State<TxList> {
    final List<Transaction> _txList = <Transaction>[];

    @override
    Widget build(BuildContext context) {
        return Scaffold(
            appBar: AppBar(title: const Text('Transactions'))
        );
    }
}
