import 'package:flutter/material.dart';
import 'package:money_matters/widgets/Drawer.dart' as customDrawer;
import 'package:money_matters/widgets/BudgetGroup.dart';
import 'package:money_matters/data.dart';

void main() => runApp(MyApp());

const String AppTitle = 'Money Matters';

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    Widget child = Home();

    child = customDrawer.Drawer(child: child);

    return MaterialApp(
      home: child,
      debugShowCheckedModeBanner: false,
      theme: ThemeData(fontFamily: 'Poppins'),
    );
  }
}

class Home extends StatefulWidget {
  Home();

  @override
  _HomeState createState() => _HomeState();
}

class _HomeState extends State<Home> {
  var _budgetGroups = <Widget>[
    BudgetGroup(),
    BudgetGroup(),
    BudgetGroup(),
    BudgetGroup(),
    BudgetGroup(),
    BudgetGroup(),
  ];

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: primaryColor,
      body: CustomScrollView(
        slivers: [
          SliverAppBar(
            expandedHeight: 150,
            pinned: true,
            shape: ContinuousRectangleBorder(
              borderRadius: BorderRadius.only(
                bottomLeft: Radius.circular(10),
                bottomRight: Radius.circular(10),
              ),
            ),
            backgroundColor: primaryColor,
          ),
          SliverList(
            delegate: SliverChildListDelegate(_budgetGroups),
          )
        ],
      ),
      floatingActionButton: FloatingActionButton(
        child: Icon(Icons.add),
        onPressed: (() {}),
        backgroundColor: Colors.lightBlueAccent,
        elevation: 4.0,
      ),
      floatingActionButtonLocation: FloatingActionButtonLocation.endFloat,
    );
  }
}

/*
class Home extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: primaryColor,
      body: CustomScrollView(
        slivers: [
          SliverAppBar(
              snap: false,
              floating: true,
            expandedHeight: 150,
            pinned: true,
            leading: IconButton(
              icon: Icon(Icons.menu),
              onPressed: () {},
            ),
            shape: ContinuousRectangleBorder(
              borderRadius: BorderRadius.only(
                bottomLeft: Radius.circular(50),
                bottomRight: Radius.circular(50),
              ),
            ),
            backgroundColor: primaryColor,
            flexibleSpace: FlexibleSpaceBar(
              centerTitle: true,
              title: Text('Title big'),
            ),
          ),
          SliverList(
            delegate: SliverChildListDelegate([
              BudgetGroup(),
              BudgetGroup(),
              BudgetGroup(),
              BudgetGroup(),
              BudgetGroup(),
              BudgetGroup(),
              BudgetGroup(),
              BudgetGroup(),
              Container(height: 50),
            ]),
          )
        ],
      ),
      floatingActionButton: FloatingActionButton(
        child: Icon(Icons.add),
        onPressed: (() {}),
        backgroundColor: Colors.lightBlueAccent,
        elevation: 4.0,
      ),
      floatingActionButtonLocation: FloatingActionButtonLocation.endFloat,
    );
  }
}
*/

class BottomBar extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return BottomAppBar(
      child: Row(
        mainAxisSize: MainAxisSize.max,
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: <Widget>[
          IconButton(icon: Icon(Icons.menu), onPressed: null),
          IconButton(icon: Icon(Icons.search), onPressed: null),
        ],
      ),
      shape: CircularNotchedRectangle(),
    );
  }
}
